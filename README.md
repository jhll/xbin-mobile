#xbin-mobile

模仿国内知名B2C网站h5移动端

使用技术:
* nuxt

# xbin-mobile

> mobile for xbin

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).


进度

* 


## bootstrap设计神器


[这个网站很不错-对于新手学习](https://www.runoob.com/try/bootstrap/layoutit/#)

## 视频记录-开发过程


[链接](./video.md)

## 后端接口

由于原来的接口项目没有维护了，导致需要很多配置才可以启动，因此重新选择了一个后端接口项目

[项目地址](https://gitee.com/zlt2000/microservices-platform?_from=gitee_search)